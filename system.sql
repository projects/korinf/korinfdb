DROP TABLE IF EXISTS system;
CREATE TABLE system (

   id INT PRIMARY KEY,
   id_distr INT,
   id_name INT NOT NULL,
   name_sys VARCHAR(255) NOT NULL,
   rel_date DATE,
   end_date DATE,
   FOREIGN KEY (id_distr) REFERENCES distr (id)
   );
