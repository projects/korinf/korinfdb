DROP TABLE IF EXISTS rel;
CREATE TABLE rel (

   id INT PRIMARY KEY,
   id_product INT,
   version VARCHAR(255) NOT NULL,
   rel_date DATE,
   end_date DATE,
   FOREIGN KEY (id_product) REFERENCES products (id)

   );
