DROP TABLE IF EXISTS rel_sys;
CREATE TABLE rel_sys (

   id_system INT,
   id_rel INT,
   FOREIGN KEY (id_system) REFERENCES system (id),
   FOREIGN KEY (id_rel) REFERENCES rel (id)

   );
